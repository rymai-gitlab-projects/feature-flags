#!/bin/sh

GITLAB_REPO=https://gitlab.com/gitlab-org/gitlab.git
CE_FLAGS_DIR=config/feature_flags/
EE_FLAGS_DIR=ee/config/feature_flags/

mkdir -p data/flags/ce data/flags/ee
cd data && rm -f flags/*.yml && rm -rf gitlab

git clone \
  --depth 1 \
  --filter=blob:none \
  --sparse \
  $GITLAB_REPO \
;

cd gitlab

# CE Flags
git sparse-checkout set $CE_FLAGS_DIR
cp -r ${CE_FLAGS_DIR}/* ../flags/ce/

# EE Flags
git sparse-checkout set $EE_FLAGS_DIR
cp -r ${EE_FLAGS_DIR}/* ../flags/ee/
