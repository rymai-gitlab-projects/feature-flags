const filtersToURL = (filters = {}) => {
  Object.entries(filters).forEach(([key, value]) => {
    if (value === undefined || value === "" || value.length === 0) {
      delete filters[key];
    }
  });

  return encodeURI(JSON.stringify(filters));
};
export default filtersToURL;
