import {
  TYPE_ENABLED,
  TYPE_GROUP,
  TYPE_MILESTONE,
  TYPE_TYPE,
} from "../constants";

import compareVersionNumbers from "../utils/compareVersionNumbers";

const isInOrder = (a, b, { type, asc }) => {
  switch (type) {
    case TYPE_MILESTONE:
      return compareVersionNumbers(
        a[TYPE_MILESTONE],
        b[TYPE_MILESTONE],
        asc ? ">" : "<"
      );
    case TYPE_GROUP:
      return asc
        ? a[TYPE_GROUP] > b[TYPE_GROUP]
        : a[TYPE_GROUP] < b[TYPE_GROUP];
    case TYPE_TYPE:
      return asc ? a[TYPE_TYPE] > b[TYPE_TYPE] : a[TYPE_TYPE] < b[TYPE_TYPE];
    case TYPE_ENABLED:
      return asc
        ? a[TYPE_ENABLED] > b[TYPE_ENABLED]
        : a[TYPE_ENABLED] < b[TYPE_ENABLED];
    default:
      return true;
  }
};

export default isInOrder;
