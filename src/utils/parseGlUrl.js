const parseGlUrl = (url) => url.match(/[^\d]*(\d*)(\/?)/)[1];

export default parseGlUrl;
